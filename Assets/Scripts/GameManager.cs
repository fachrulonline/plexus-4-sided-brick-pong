﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public int lives;
    public int score;
    public Text livesText, scoreText;
    public bool isGameover;
    public GameObject gameOverPanel;
    public int numberOfBrick;
    public Slider slider;

    // Start is called before the first frame update
    void Start()
    {
        livesText.text = "Lives: "+lives;
        scoreText.text = "Score: "+score;
        numberOfBrick = GameObject.FindGameObjectsWithTag("Bata").Length;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateLives(int changeInLives){
        lives += changeInLives;
        if(lives <= 0)
        {
            lives = 0;
            GameOver();
        }

        livesText.text = "Lives: "+lives;
    }

    public void UpdateScore(int point){
        score += point;

        scoreText.text = "Score: "+score;
    }

    public void UpdateNumberOfBrick()
    {
        numberOfBrick--;
        if(numberOfBrick <= 0)
        {
            GameOver();
        }
    }

    private void GameOver()
    {
        isGameover = true;
        gameOverPanel.SetActive(true);
    }

    public void PlayAgain()
    {
        SceneManager.LoadScene("MainGame");
    }

    public void Quit()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void checkingBolaBonus()
    {
        bool isStillHaveMainBola;

        GameObject[] allBola = GameObject.FindGameObjectsWithTag("Bola");
        foreach (var item in allBola)
        {
                
        }   
    }
}
