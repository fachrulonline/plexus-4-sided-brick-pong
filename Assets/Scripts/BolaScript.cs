﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BolaScript : MonoBehaviour
{
    public Rigidbody2D rb;
    public bool inPlay;
    public Transform papan;
    public float speed;
    public Transform pecahanBata;
    public GameManager gameManager;
    Touch touch;
    public Transform bonus;
    public bool isBonusBola;


    // Start is called before the first frame update
    void Start()
    {
        setInitialBola();
    }

    // Update is called once per frame
    void Update()
    {
        if (gameManager.isGameover)
        {
            return;
        }

        if (!inPlay)
        {
            transform.position = papan.position;
        }
    }

    public void BolaStartForce()
    {
        if(!inPlay)
        {
            inPlay = true;
            rb.AddForce(Vector2.up * speed);
        }   
    }

    public void setInitialBola(){
        rb = this.gameObject.GetComponent<Rigidbody2D>();    
        gameManager = GameObject.Find("Game Manager").GetComponent<GameManager>();
        papan = GameObject.FindGameObjectWithTag("PapanUtama").transform.GetChild(0).transform;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Batas") && (!isBonusBola)){
            Debug.Log("Bola kena ke ujung screen");
            rb.velocity = Vector2.zero;
            inPlay = false;
            gameManager.UpdateLives(-1);
        }
    }

    void OnCollisionEnter2D(Collision2D other) {
        if(other.transform.CompareTag("Bata"))
        {
            int randomPercentChanceBonus = Random.Range(1,101);

            if(randomPercentChanceBonus < 5)
            {
                Instantiate(bonus, other.transform.position, other.transform.rotation);
            }

            Transform pecahanBataTerbaru =  Instantiate(pecahanBata, other.transform.position, other.transform.rotation);
            Destroy(pecahanBataTerbaru.gameObject, 2.5f);

            gameManager.UpdateScore(other.gameObject.GetComponent<BataScript>().points);
            gameManager.UpdateNumberOfBrick();
            Destroy(other.gameObject);
        }    
    }
}
