﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusScript : MonoBehaviour
{
    public float speed;
    public GameObject bola;
    public int countBolaBonus;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(new Vector2(0f, -1f) * Time.deltaTime * speed);

        if(transform.position.y < -10f)
        {
            Destroy(this.gameObject);
        }
    }

    public void activeBonus()
    {
        for (int i = 0; i < countBolaBonus; i++)
        {
            GameObject.Instantiate(bola, bola.transform.position, Quaternion.identity);
        }
        forceSemuaBola();

        Destroy(this.gameObject);
    }

    void forceSemuaBola()
    {
        GameObject[] allBola = GameObject.FindGameObjectsWithTag("Bola");
        foreach (var item in allBola)
        {   
            item.GetComponent<BolaScript>().setInitialBola();
            item.GetComponent<BolaScript>().isBonusBola = true;
            item.GetComponent<BolaScript>().BolaStartForce();
        }
    }    

}
