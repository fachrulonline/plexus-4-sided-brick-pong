﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PapanScript : MonoBehaviour
{

    public float speed;
    public float rightScreenEdge;
    public float leftScreenEdge;
    public float topScreenEdge;
    public float bottomScreenEdge;
    public GameManager gameManager;
    
    [Range(0,3)]
    public int jenisPapan;
    
    public float horizontal;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(gameManager.isGameover)
        {
            return;
        }

        horizontal = gameManager.slider.value;

        // Debug.Log("horizontal: "+horizontal);
        
        if(jenisPapan <= 1)
        {   
            this.transform.position = new Vector2(horizontal, this.transform.position.y);
        }else
        {
            float newY = (horizontal * (topScreenEdge - bottomScreenEdge)) / (rightScreenEdge - leftScreenEdge);
            this.transform.position = new Vector2(this.transform.position.x, (newY));
        }
        
       
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("kena "+other.name);
        other.GetComponent<BonusScript>().activeBonus();
    }
}
